#include <iostream>

int main()
{
  int a, b, sum=0;
  std::cout << "Enter 2 numbers: ";
  std::cin >> a >> b;

  for (int i=a; i<=b; i++)
    {
      sum = sum + 1;
    }
  
  std::cout << "Sum from" << a << "to" << b << "(both inclusive) is " << sum;
  return 0;
}  
